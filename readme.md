# Quoi de neuf ?

**version 4.0.0**
 + Migration en Laminas

**version 0.7.0** 
 + intégration des premières tables liées à l'offre de formation
    + ODF_DIPLOME => OffreDeFormationDiplome
    + ODF_ETAPE => OffreDeFormationEtape
 + récupération des inscriptions et ajout des liens avec la table individu
    + INDIVIDU_INSCRIPTION => IndividuInscription 
 + Création du service OffreDeFormationService pour récupérer les informations associées à l'offre de formation
 
 **version 0.6.0**
 
 + intégration des fonctions
