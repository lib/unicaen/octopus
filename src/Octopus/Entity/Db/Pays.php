<?php

namespace Octopus\Entity\Db;

use DateTime;
use UnicaenApp\Entity\HistoriqueAwareTrait;

class Pays {
    use HistoriqueAwareTrait;

    /** @var integer */
    private $id;
    /** @var string */
    private $codePays;
    /** @var integer */
    private $paysRattachement;
    /** @var string */
    private $libelleCourt;
    /** @var string */
    private $libelleLong;
    /** @var string */
    private $codeIso2;
    /** @var string */
    private $codeIso3;
    /** @var string */
    private $codeIsoNum;
    /** @var DateTime */
    private $dateOuverture;
    /** @var DateTime */
    private $dateFermeture;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCodePays()
    {
        return $this->codePays;
    }

    /**
     * @return int
     */
    public function getPaysRattachement()
    {
        return $this->paysRattachement;
    }

    /**
     * @return string
     */
    public function getLibelleCourt()
    {
        return $this->libelleCourt;
    }

    /**
     * @return string
     */
    public function getLibelleLong()
    {
        return $this->libelleLong;
    }

    /**
     * @return string
     */
    public function getCodeIso2()
    {
        return $this->codeIso2;
    }

    /**
     * @return string
     */
    public function getCodeIso3()
    {
        return $this->codeIso3;
    }

    /**
     * @return string
     */
    public function getCodeIsoNum()
    {
        return $this->codeIsoNum;
    }

    /**
     * @return DateTime
     */
    public function getDateOuverture()
    {
        return $this->dateOuverture;
    }

    /**
     * @return DateTime
     */
    public function getDateFermeture()
    {
        return $this->dateFermeture;
    }

    public function __toString()
    {
        $texte  = $this->getLibelleLong();
        return $texte;
    }
}