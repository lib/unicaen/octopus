<?php

namespace Octopus\Entity\Db;

use DateTime;

class IndividuAffectation {

    /** @var integer */
    private $id;
    /** @var Individu */
    private $individu;
    /** @var Structure */
    private $structure;
    /** @var IndividuAffectationType */
    private $type;
    /** @var Source */
    private $source;
    /** @var DateTime */
    private $dateDebut;
    /** @var DateTime */
    private $dateFin;
    /** @var integer */
    private $idOrig;
    /** @var boolean */
    private $principal;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Individu
     */
    public function getIndividu()
    {
        return $this->individu;
    }

    /**
     * @return Structure
     */
    public function getStructure()
    {
        return $this->structure;
    }

    /**
     * @return IndividuAffectationType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return Source
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @return DateTime
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * @return DateTime
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * @return int
     */
    public function getIdOrig()
    {
        return $this->idOrig;
    }

    /**
     * @return bool
     */
    public function isPrincipal()
    {
        return $this->principal;
    }


}