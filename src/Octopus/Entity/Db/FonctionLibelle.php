<?php

namespace Octopus\Entity\Db;

class FonctionLibelle {
    /** @var integer */
    private $id;
    /** @var Fonction */
    private $fonction;
    /** @var string */
    private $libelle;
    /** @var string */
    private $genre;
    /** @var string */
    private $default;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Fonction
     */
    public function getFonction()
    {
        return $this->fonction;
    }

    /**
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @return string
     */
    public function getGenre()
    {
        return $this->genre;
    }

    /**
     * @return boolean
     */
    public function getDefault()
    {
        return ($this->default === 'O');
    }


}