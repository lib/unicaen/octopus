<?php

namespace Octopus\Entity\Db;

use DateTime;

class IndividuCompte {

    /** @var integer */
    private $id;
    /** @var Individu */
    private $individu;
    /** @var IndividuCompteType */
    private $type;
    /** @var string */
    private $login;
    /** @var string */
    private $email;
    private ?int $statut = null;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Individu
     */
    public function getIndividu(): Individu
    {
        return $this->individu;
    }

    /**
     * @return IndividuCompteType
     */
    public function getType(): IndividuCompteType
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return int|null
     */
    public function getStatut(): ?int
    {
        return $this->statut;
    }



}