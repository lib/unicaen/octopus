<?php

namespace Octopus\Entity\Db;

use DateTime;

class IndividuCompteType {

    const INDIVIDU_COMPTE_ADMINISTRATEUR = "administrateur";
    const INDIVIDU_COMPTE_UTILISATEUR = "utilisateur";

    /** @var integer */
    private $id;
    /** @var string */
    private $nom;
    /** @var string */
    private $libelle;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * @return string
     */
    public function getLibelle(): string
    {
        return $this->libelle;
    }

}