<?php

namespace Octopus\Entity\Db;

use Doctrine\Common\Collections\ArrayCollection;

class Fonction {
    /** @var integer */
    private $id;
    /** @var Fonction */
    private $parent;
    /** @var ArrayCollection */
    private $enfants;
    /** @var FonctionType */
    private $type;
    /** @var string */
    private $code;
    /** @var string */
    private $codeSource;
    /** @var integer */
    private $niveau;
    /** @var ArrayCollection */
    private $libelles;

    public function __construct()
    {
        $this->enfants = new ArrayCollection();
        $this->libelles = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Fonction
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @return Fonction[]
     */
    public function getEnfants()
    {
        return $this->enfants->toArray();
    }

    /**
     * @return FonctionType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getCodeSource()
    {
        return $this->codeSource;
    }

    /**
     * @return int
     */
    public function getNiveau()
    {
        return $this->niveau;
    }

    /**
     * @return FonctionLibelle[]
     */
    public function getLibelles()
    {
        return $this->libelles->toArray();
    }

}