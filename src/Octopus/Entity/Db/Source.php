<?php

namespace Octopus\Entity\Db;

class Source {

    const SOURCE_APOGEE = "APO";
    const SOURCE_HARPEGE = "HARP";
    const SOURCE_FCA = "FCA";
    const SOURCE_OCTO = "OCTO";
    const SOURCE_INV = "INV";


    /** @var string */
    private $cSource;
    /** @var string */
    private $libelleCourt;
    /** @var string */
    private $libelleLong;
    /** @var string */
    private $tableLocale;
    /** @var int */
    private $priorite;

    /**
     * @return string
     */
    public function getCSource()
    {
        return $this->cSource;
    }

    /**
     * @return string
     */
    public function getLibelleCourt()
    {
        return $this->libelleCourt;
    }

    /**
     * @return string
     */
    public function getLibelleLong()
    {
        return $this->libelleLong;
    }

    /**
     * @return string
     */
    public function isTableLocale()
    {
        return ($this->tableLocale === "O");
    }

    /**
     * @return int
     */
    public function getPriorite()
    {
        return $this->priorite;
    }

    public function __toString()
    {
        switch ($this->cSource) {
            case self::SOURCE_OCTO      :  return '<span class="badge" style="background-color: darkred;">'. $this->getLibelleLong() .'</span>';
            case self::SOURCE_HARPEGE   :  return '<span class="badge" style="background-color: hotpink;">'. $this->getLibelleLong() .'</span>';
            case self::SOURCE_APOGEE    :  return '<span class="badge" style="background-color: royalblue;">'. $this->getLibelleLong() .'</span>';
            case self::SOURCE_FCA       :  return '<span class="badge" style="background-color: cadetblue;">'. $this->getLibelleLong() .'</span>';
            case self::SOURCE_INV       :  return '<span class="badge" style="background-color: darkgreen;">'. $this->getLibelleLong() .'</span>';
            default                     :  return '<span class="badge" style="background-color: gray;">'. $this->getLibelleLong() .'</span>';
        }
    }


}