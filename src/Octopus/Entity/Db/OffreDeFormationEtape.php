<?php

namespace Octopus\Entity\Db;

class OffreDeFormationEtape {

    /** @var integer */
    private $id;
    /** @var Structure */
    private $structure;
    /** @var string */
    private $supprime;
    /** @var string */
    private $libelleCourt;
    /** @var string */
    private $libelleLong;
    /** @var string */
    private $anneeDebut;
    /** @var string */
    private $anneeFin;
    /** @var string */
    private $diplomante;
    /** @var string */
    private $aDistance;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Structure
     */
    public function getStructure()
    {
        return $this->structure;
    }

    /**
     * @return boolean|null
     */
    public function isSupprime()
    {
        if ($this->supprime === 'O') return true;
        if ($this->supprime === 'N') return false;
        return null;
    }

    /**
     * @return string
     */
    public function getLibelleCourt()
    {
        return $this->libelleCourt;
    }

    /**
     * @return string
     */
    public function getLibelleLong()
    {
        return $this->libelleLong;
    }

    /**
     * @return string
     */
    public function getAnneeDebut()
    {
        return $this->anneeDebut;
    }

    /**
     * @return string
     */
    public function getAnneeFin()
    {
        return $this->anneeFin;
    }

    /**
     * @return boolean|null
     */
    public function getDiplomante()
    {
        if ($this->diplomante === 'O') return true;
        if ($this->diplomante === 'N') return false;
        return null;
    }

    /**
     * @return boolean|null
     */
    public function getADistance()
    {
        if ($this->aDistance === 'O') return true;
        if ($this->aDistance === 'N') return false;
        return null;
    }

}