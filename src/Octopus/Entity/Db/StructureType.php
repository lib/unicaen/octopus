<?php

namespace Octopus\Entity\Db;

class StructureType {

    const TYPE_ANTN = 'ANTN';
    const TYPE_BIBL	= 'BIBL';
    const TYPE_COMP	= 'COMP';
    const TYPE_DEPT	= 'DEPT';
    const TYPE_ED	= 'ED';
    const TYPE_ETAB	= 'ETAB';
    const TYPE_FICT	= 'FICT';
    const TYPE_SCEN	= 'SCEN';
    const TYPE_SCOM	= 'SCOM';
    const TYPE_SREC	= 'SREC';
    const TYPE_SSAD	= 'SSAD';

    /** @var integer */
    private $id;
    /** @var string */
    private $code;
    /** @var string */
    private $libelle;
    /** @var string */
    private $description;

    /**
     * @return int
     */
    public function  getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    public function __toString()
    {
        $texte   = '<span class="badge" style="background-color: slategray; border-radius: 0;">';
        $texte  .= $this->getLibelle();
        $texte  .= '</span>';
        return $texte;
    }

}