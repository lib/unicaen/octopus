<?php

namespace Octopus\Entity\Db;

class IndividuInscription {

    /** @var int */
    private $id;
    /** @var Individu */
    private $individu;
    /** @var OffreDeFormationEtape */
    private $etape;
    /** @var OffreDeFormationDiplome */
    private $diplome;
    /** @var string */
    private $annee;
    /** @var string */
    private $etatInscription;
    /** @var string */
    private $regimeSISE;
    /** @var string */
    private $obtenuDip;
    /** @var string */
    private $obtenuVet;
    /** @var string */
    private $aDistance;
    private ?string $regime = null;
    private ?string $regimeCode = null;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Individu
     */
    public function getIndividu()
    {
        return $this->individu;
    }

    /**
     * @return OffreDeFormationEtape
     */
    public function getEtape()
    {
        return $this->etape;
    }

    /**
     * @return OffreDeFormationDiplome
     */
    public function getDiplome()
    {
        return $this->diplome;
    }

    /**
     * @return string
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * @return string
     */
    public function getEtatInscription()
    {
        return $this->etatInscription;
    }

    /**
     * @return string
     */
    public function getRegimeSISE()
    {
        return $this->regimeSISE;
    }

    /**
     * @return boolean|null
     */
    public function isObtenu()
    {
        if ($this->obtenuDip === 'O' OR $this->obtenuVet === 'O') return true;
        if ($this->obtenuDip === 'N' OR $this->obtenuVet === 'N') return false;
        return null;
    }

    /**
     * @return boolean|null
     */
    public function isADistance()
    {
        if ($this->aDistance === 'O') return true;
        if ($this->aDistance === 'N') return false;
        return null;
    }

    public function getRegime(): ?string
    {
        return $this->regime;
    }

    public function getRegimeCode(): ?string
    {
        return $this->regimeCode;
    }


}