<?php

namespace Octopus\Entity\Db;

// TODO prendre les overwrites, normalisés, phonétiques
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;

class Individu {
    /** @var integer */
    private $cIndividuChaine;
    /** @var Source */
    private $cSource;
    /** @var integer */
    private $cEtu;
    /** @var integer */
    private $cIne;
    /** @var string */
    private $sexe;
    /** @var string */
    private $prenom;
    /** @var string */
    private $prenom_ow;
    /** @var string */
    private $prenom2;
    /** @var string */
    private $prenom3;
    /** @var string */
    private $nomFamille;
    /** @var string */
    private $nomFamille_ow;
    /** @var string */
    private $nomUsage;
    /** @var string */
    private $nomUsage_ow;
    /** @var string */
    private $dateNaissance;
    /** @var string */
    private $villeNaissance;
    /** @var string */
    private $cCommuneNaissance;
    /** @var string */
    private $cDeptNaissance;
    /** @var string */
    private $cPaysNaissance;
    /** @var string */
    private $cPaysNationalite;
    /** @var string */
    private $telPersonnelle;
    /** @var string */
    private $emailPersonnelle;
    /** @var DateTime */
    private $dateModification;

    /** @var ArrayCollection (IndividuAffectation) */
    private $affectations;
    /** @var ArrayCollection (IndividuCompte) */
    private $comptes;
    /** @var ArrayCollection (IndividuInscription) */
    private $inscriptions;

    public function __construct()
    {
        $this->affectations = new ArrayCollection();
        $this->comptes = new ArrayCollection();
        $this->inscriptions = new ArrayCollection();
    }


    /**
     * @return int
     */
    public function getCIndividuChaine()
    {
        return $this->cIndividuChaine;
    }

    /**
     * @return Source
     */
    public function getCSource()
    {
        return $this->cSource;
    }

    /**
     * @return int
     */
    public function getCEtu()
    {
        return $this->cEtu;
    }

    /**
     * @return int
     */
    public function getCIne()
    {
        return $this->cIne;
    }

    /**
     * @return string
     */
    public function getSexe()
    {
        return $this->sexe;
    }

    /**
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom_ow??$this->prenom;
    }

    /**
     * @return string
     */
    public function getPrenom2()
    {
        return $this->prenom2;
    }

    /**
     * @return string
     */
    public function getPrenom3()
    {
        return $this->prenom3;
    }

    /**
     * @return string
     */
    public function getNomFamille()
    {
        return $this->nomFamille_ow??$this->nomFamille;
    }

    /**
     * @return string
     */
    public function getNomUsage()
    {
        return $this->nomUsage_ow??$this->nomFamille;
    }

    /**
     * @return string
     */
    public function getDateNaissance()
    {
        return $this->dateNaissance;
    }

    /**
     * @return string
     */
    public function getVilleNaissance()
    {
        return $this->villeNaissance;
    }

    /**
     * @return string
     */
    public function getCCommuneNaissance()
    {
        return $this->cCommuneNaissance;
    }

    /**
     * @return string
     */
    public function getCDeptNaissance()
    {
        return $this->cDeptNaissance;
    }

    /**
     * @return string
     */
    public function getCPaysNaissance()
    {
        return $this->cPaysNaissance;
    }

    /**
     * @return string
     */
    public function getCPaysNationalite()
    {
        return $this->cPaysNationalite;
    }

    /**
     * @return string
     */
    public function getTelPersonnelle()
    {
        return $this->telPersonnelle;
    }

    /**
     * @return string
     */
    public function getEmailPersonnelle()
    {
        return $this->emailPersonnelle;
    }

    /**
     * @return DateTime
     */
    public function getDateModification()
    {
        return $this->dateModification;
    }

    /**
     * @param boolean $active
     * @return IndividuAffectation[]
     */
    public function getAffectations($active = false) {
        $affectations = $this->affectations->toArray();

        if ($active) {
            $affectations = array_filter($affectations, function(IndividuAffectation $a) { return ($a->getDateFin() === null);});
        }
        usort($affectations, function(IndividuAffectation $a, IndividuAffectation $b) { return $a->getDateDebut() > $b->getDateDebut();});
        return $affectations;
    }

    /**
     * @return IndividuInscription[]
     */
    public function getInscriptions()
    {
        $inscriptions = $this->inscriptions->toArray();
        usort($inscriptions, function (IndividuInscription $a, IndividuInscription $b) { return $a->getAnnee() > $b->getAnnee();});
        return $inscriptions;
    }

    /**
     * @return IndividuCompte[]
     */
    public function getComptes() : array
    {
        $comptes = $this->comptes->toArray();
        array_filter($comptes, function(IndividuCompte $a) { return $a->getType()->getNom() === IndividuCompteType::INDIVIDU_COMPTE_UTILISATEUR;});
        return $comptes;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $texte  = $this->getPrenom();
        if ($this->getNomUsage()) $texte .= " " . $this->getNomUsage();
        if ($this->getNomUsage() != $this->getNomFamille()) {
            if ($this->getNomUsage()) $texte .= "-"; else $texte .= " ";
            $texte .= $this->getNomFamille();
        }
        return $texte;
    }
}