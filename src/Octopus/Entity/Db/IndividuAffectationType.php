<?php

namespace Octopus\Entity\Db;

class IndividuAffectationType {
    /** @var integer */
    private $id;
    /** @var string */
    private $nom;
    /** @var string */
    private $libelle;
    /** @var string */
    private $description;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    public function __toString()
    {
        return $this->getLibelle();
    }


}
