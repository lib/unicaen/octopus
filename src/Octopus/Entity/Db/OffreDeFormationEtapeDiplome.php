<?php

namespace Octopus\Entity\Db;

class OffreDeFormationEtapeDiplome {

    /** @var OffreDeFormationEtape */
    private $etape;
    /** @var OffreDeFormationDiplome */
    private $diplome;
    /** @var string */
    private $cursus;
    /** @var string */
    private $niveau;

    /**
     * @return OffreDeFormationEtape
     */
    public function getEtape()
    {
        return $this->etape;
    }

    /**
     * @return OffreDeFormationDiplome
     */
    public function getDiplome()
    {
        return $this->diplome;
    }

    /**
     * @return string
     */
    public function getCursus()
    {
        return $this->cursus;
    }

    /**
     * @return string
     */
    public function getNiveau()
    {
        return $this->niveau;
    }
}