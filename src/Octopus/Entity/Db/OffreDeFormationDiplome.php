<?php

namespace Octopus\Entity\Db;

class OffreDeFormationDiplome {

    /** @var integer */
    private $id;
    /** @var string */
    private $supprime;
    /** @var string */
    private $libelleCourt;
    /** @var string */
    private $libelleLong;
    /** @var string */
    private $typeDiplomeSISE;
    /** @var string */
    private $codeDiplomeSISE;
    /** @var string */
    private $secteurDiscplineSISE;
    /** @var string */
    private $anneeDebut;
    /** @var string */
    private $anneeFin;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return boolean|null
     */
    public function isSupprime()
    {
        if ($this->supprime === 'O') return true;
        if ($this->supprime === 'N') return false;
        return null;
    }

    /**
     * @return string
     */
    public function getLibelleCourt()
    {
        return $this->libelleCourt;
    }

    /**
     * @return string
     */
    public function getLibelleLong()
    {
        return $this->libelleLong;
    }

    /**
     * @return string
     */
    public function getTypeDiplomeSISE()
    {
        return $this->typeDiplomeSISE;
    }

    /**
     * @return string
     */
    public function getCodeDiplomeSISE()
    {
        return $this->codeDiplomeSISE;
    }

    /**
     * @return string
     */
    public function getSecteurDiscplineSISE()
    {
        return $this->secteurDiscplineSISE;
    }

    /**
     * @return string
     */
    public function getAnneeDebut()
    {
        return $this->anneeDebut;
    }

    /**
     * @return string
     */
    public function getAnneeFin()
    {
        return $this->anneeFin;
    }

}