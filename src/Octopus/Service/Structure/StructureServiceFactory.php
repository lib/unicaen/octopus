<?php

namespace Octopus\Service\Structure;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;

class StructureServiceFactory {

    public function __invoke(ContainerInterface $container)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get('doctrine.entitymanager.orm_octopus');

        /** @var StructureService $service */
        $service = new StructureService();
        $service->setEntityManager($entityManager);
        return $service;
    }
}