<?php

namespace Octopus\Service\Geographie;

use Doctrine\ORM\NonUniqueResultException;
use Octopus\Entity\Db\Pays;
use UnicaenApp\Exception\RuntimeException;
use UnicaenApp\Service\EntityManagerAwareTrait;

class GeographieService {
    use EntityManagerAwareTrait;

    /**
     * @param string $order
     * @return Pays[]
     */
    public function getPays($order = null)
    {
        $qb = $this->getEntityManager()->getRepository(Pays::class)->createQueryBuilder('pays');

        if($order) $qb = $qb->orderBy('pays.' . $order);

        $qb = $qb->setMaxResults(501);

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /**
     * @param integer $id
     * @return Pays
     */
    public function getPay($id)
    {
        $qb = $this->getEntityManager()->getRepository(Pays::class)->createQueryBuilder('pays')
            ->andWhere('pays.id = :id')
            ->setParameter('id', $id)
        ;

        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Pays partagent le même identifiant [".$id."].");
        }
        return $result;
    }

    /**
     * @param string $term
     * @return Pays[]
     */
    public function getPaysByTerm($term)
    {
        $qb = $this->getEntityManager()->getRepository(Pays::class)->createQueryBuilder('pays')
            ->andWhere('pays.libelleCourt LIKE :search')
            ->setParameter('search', '%'.$term.'%')
            ->orderBy('pays.libelleCourt')
        ;

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /**
     * @param string $code
     * @return Pays
     */
    public function getPaysByCode($code) {
        $qb = $this->getEntityManager()->getRepository(Pays::class)->createQueryBuilder('pays')
            ->andWhere('pays.codePays = :code')
            ->setParameter('code', $code)
        ;

        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Pays partagent le même code [".$code."].");
        }
        return $result;
    }
}