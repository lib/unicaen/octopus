<?php

namespace Octopus\Service\Geographie;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;

class GeographieServiceFactory {

    public function __invoke(ContainerInterface $container)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get('doctrine.entitymanager.orm_octopus');

        /** @var GeographieService $service */
        $service = new GeographieService();
        $service->setEntityManager($entityManager);
        return $service;
    }
}