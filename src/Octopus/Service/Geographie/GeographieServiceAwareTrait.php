<?php

namespace Octopus\Service\Geographie;

trait GeographieServiceAwareTrait {

    /** @var GeographieService */
    private $geographieService;

    /**
     * @return GeographieService
     */
    public function getGeographieService()
    {
        return $this->geographieService;
    }

    /**
     * @param GeographieService $geographieService
     * @return GeographieService
     */
    public function setGeographieService($geographieService)
    {
        $this->geographieService = $geographieService;
        return $this->geographieService;
    }


}