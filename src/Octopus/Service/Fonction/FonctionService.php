<?php

namespace Octopus\Service\Fonction;

use Doctrine\ORM\NonUniqueResultException;
use Octopus\Entity\Db\Fonction;
use Octopus\Entity\Db\FonctionLibelle;
use Octopus\Entity\Db\FonctionType;
use UnicaenApp\Exception\RuntimeException;
use UnicaenApp\Service\EntityManagerAwareTrait;

class FonctionService {
    use EntityManagerAwareTrait;

    /** FONCTION ******************************************************************************************************/

    /**
     * @param string $order
     * @return Fonction[]
     */
    public function getFonctions($order = null)
    {
        $qb = $this->getEntityManager()->getRepository(Fonction::class)->createQueryBuilder('fonction')
            ->addSelect('libelle')->join('fonction.libelles', 'libelle')
            ->addSelect('type')->join('fonction.type', 'type')
        ;

        if($order) $qb = $qb->orderBy('fonction.' . $order);

        $qb = $qb->setMaxResults(501);

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /**
     * @param integer $id
     * @return Fonction
     */
    public function getFonction($id)
    {
        $qb = $this->getEntityManager()->getRepository(Fonction::class)->createQueryBuilder('fonction')
            ->addSelect('libelle')->join('fonction.libelles', 'libelle')
            ->addSelect('type')->join('fonction.type', 'type')
            ->andWhere('fonction.id = :id')
            ->setParameter('id', $id)
        ;

        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Fonction partagent le même identifiant [".$id."].");
        }
        return $result;
    }

    /**
     * @param string $term
     * @return Fonction[]
     */
    public function getFonctionsByTerm($term)
    {
        $qb = $this->getEntityManager()->getRepository(Fonction::class)->createQueryBuilder('fonction')
            ->addSelect('libelle')->join('fonction.libelles', 'libelle')
            ->addSelect('type')->join('fonction.type', 'type')
            ->andWhere('libelle.libelle LIKE :search')
            ->setParameter('search', '%'.$term.'%')
            ->orderBy('fonction.libelle')
        ;

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /** FONCTION_LIBELLE **********************************************************************************************/

    /**
     * @param string $order
     * @return Fonction[]
     */
    public function getFonctionsLibelles($order = null)
    {
        $qb = $this->getEntityManager()->getRepository(FonctionLibelle::class)->createQueryBuilder('libelle')
            ->addSelect('fonction')->join('libelle.fonction', 'fonction')
        ;

        if($order) $qb = $qb->orderBy('libelle.' . $order);

        $qb = $qb->setMaxResults(501);

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /**
     * @param integer $id
     * @return Fonction
     */
    public function getFonctionLibelle($id)
    {
        $qb = $this->getEntityManager()->getRepository(FonctionLibelle::class)->createQueryBuilder('libelle')
            ->addSelect('fonction')->join('libelle.fonction', 'fonction')
            ->andWhere('libelle.id = :id')
            ->setParameter('id', $id)
        ;

        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs FonctionLibelle partagent le même identifiant [".$id."].");
        }
        return $result;
    }

    /**
     * @param string $term
     * @return Fonction[]
     */
    public function getFonctionsLibellesByTerm($term)
    {
        $qb = $this->getEntityManager()->getRepository(FonctionLibelle::class)->createQueryBuilder('libelle')
            ->addSelect('fonction')->join('libelle.fonction', 'fonction')
            ->andWhere('libelle.libelle LIKE :search')
            ->setParameter('search', '%'.$term.'%')
            ->orderBy('libelle.libelle')
        ;

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /** FONCTION_TYPE *************************************************************************************************/

    /**
     * @param string $order
     * @return Fonction[]
     */
    public function getFonctionsTypes($order = null)
    {
        $qb = $this->getEntityManager()->getRepository(FonctionType::class)->createQueryBuilder('type')
        ;

        if($order) $qb = $qb->orderBy('type.' . $order);

        $qb = $qb->setMaxResults(501);

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /**
     * @param integer $id
     * @return Fonction
     */
    public function getFonctionType($id)
    {
        $qb = $this->getEntityManager()->getRepository(FonctionType::class)->createQueryBuilder('type')
            ->andWhere('type.id = :id')
            ->setParameter('id', $id)
        ;

        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs FonctionType partagent le même identifiant [".$id."].");
        }
        return $result;
    }

    /**
     * @param string $term
     * @return Fonction[]
     */
    public function getFonctionsTypesByTerm($term)
    {
        $qb = $this->getEntityManager()->getRepository(FonctionType::class)->createQueryBuilder('type')
            ->andWhere('type.libelle LIKE :search')
            ->setParameter('search', '%'.$term.'%')
            ->orderBy('type.libelle')
        ;

        $result = $qb->getQuery()->getResult();
        return $result;
    }
}