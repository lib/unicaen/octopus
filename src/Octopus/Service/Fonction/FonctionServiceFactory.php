<?php

namespace Octopus\Service\Fonction;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;

class FonctionServiceFactory {

    public function __invoke(ContainerInterface $container)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get('doctrine.entitymanager.orm_octopus');

        /** @var FonctionService $service */
        $service = new FonctionService();
        $service->setEntityManager($entityManager);
        return $service;
    }
}