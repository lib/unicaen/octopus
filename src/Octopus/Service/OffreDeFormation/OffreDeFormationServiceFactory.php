<?php

namespace Octopus\Service\OffreDeFormation;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;

class OffreDeFormationServiceFactory {

    /**
     * @param ContainerInterface $container
     * @return OffreDeFormationService
     */
    public function __invoke(ContainerInterface $container)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get('doctrine.entitymanager.orm_octopus');

        /** @var OffreDeFormationService $service */
        $service = new OffreDeFormationService();
        $service->setEntityManager($entityManager);
        return $service;
    }
}