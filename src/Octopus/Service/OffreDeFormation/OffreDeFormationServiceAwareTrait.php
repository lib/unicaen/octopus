<?php

namespace Octopus\Service\OffreDeFormation;

trait OffreDeFormationServiceAwareTrait {

    /** @var OffreDeFormationService */
    private $offreDeFormationService;

    /**
     * @return OffreDeFormationService
     */
    public function getOffreDeFormationService()
    {
        return $this->offreDeFormationService;
    }

    /**
     * @param OffreDeFormationService $offreDeFormationService
     * @return OffreDeFormationService
     */
    public function setOffreDeFormationService($offreDeFormationService)
    {
        $this->offreDeFormationService = $offreDeFormationService;
        return $this->offreDeFormationService;
    }
}