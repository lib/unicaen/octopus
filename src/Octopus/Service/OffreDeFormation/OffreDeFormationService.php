<?php

namespace Octopus\Service\OffreDeFormation;

use Doctrine\ORM\NonUniqueResultException;
use Octopus\Entity\Db\OffreDeFormationEtapeDiplome;
use UnicaenApp\Exception\RuntimeException;
use UnicaenApp\Service\EntityManagerAwareTrait;
use Octopus\Entity\Db\OffreDeFormationDiplome;
use Octopus\Entity\Db\OffreDeFormationEtape;

class OffreDeFormationService {
    use EntityManagerAwareTrait;

    /** DIPLOME *******************************************************************************************************/

    /**
     * @param bool $supprime
     * @param string $champ
     * @param string $ordre
     * @return OffreDeFormationDiplome[]
     */
    public function getDiplomes($supprime = false, $champ = "libelleLong", $ordre = 'ASC')
    {
        $qb = $this->getEntityManager()->getRepository(OffreDeFormationDiplome::class)->createQueryBuilder('diplome')
            ->orderBy('diplome.' . $champ, $ordre)
        ;

        if ($supprime === false) {
            $qb = $qb->andWhere('diplome.supprime = :supprimer')
                ->setParameter('supprimer', 'N');
        }
        
        $result = $qb->getQuery()->getResult();
        return $result;
    }
    
    /**
     * @param integer $id
     * @return OffreDeFormationDiplome
     */
    public function getDiplome($id)
    {
        $qb = $this->getEntityManager()->getRepository(OffreDeFormationDiplome::class)->createQueryBuilder('diplome')
            ->andWhere('diplome.id = :id')
            ->setParameter('id', $id);

        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs OffreDeFormationDiplome partagent le même identifiant [".$id."].");
        }
        return $result;
    }
    
    /**
     * @param string $term
     * @param string $champ
     * @param string $ordre
     * @return OffreDeFormationDiplome[]
     */
    public function getDiplomesByTerm($term, $champ = "libelleLong", $ordre = 'ASC')
    {
        $qb = $this->getEntityManager()->getRepository(OffreDeFormationDiplome::class)->createQueryBuilder('diplome')
            ->andWhere('diplome.libelleLong LIKE :search')
            ->setParameter('search', '%'.$term.'%')
            ->orderBy('diplome.' . $champ, $ordre)
        ;

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /** ETAPE *******************************************************************************************************/

    /**
     * @param bool $supprime
     * @param string $champ
     * @param string $ordre
     * @return OffreDeFormationDiplome[]
     */
    public function getEtapes($supprime = false, $champ = "libelleLong", $ordre = 'ASC')
    {
        $qb = $this->getEntityManager()->getRepository(OffreDeFormationEtape::class)->createQueryBuilder('etape')
            ->orderBy('etape.' . $champ, $ordre)
        ;

        if ($supprime === false) {
            $qb = $qb->andWhere('etape.supprime = :supprimer')
                ->setParameter('supprimer', 'N');
        }

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /**
     * @param integer $id
     * @return OffreDeFormationDiplome
     */
    public function getEtape($id)
    {
        $qb = $this->getEntityManager()->getRepository(OffreDeFormationEtape::class)->createQueryBuilder('etape')
            ->andWhere('etape.id = :id')
            ->setParameter('id', $id);

        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs OffreDeFormationEtape partagent le même identifiant [".$id."].");
        }
        return $result;
    }

    /**
     * @param string $term
     * @param string $champ
     * @param string $ordre
     * @return OffreDeFormationDiplome[]
     */
    public function getEtapesByTerm($term, $champ = "libelleLong", $ordre = 'ASC')
    {
        $qb = $this->getEntityManager()->getRepository(OffreDeFormationEtape::class)->createQueryBuilder('etape')
            ->andWhere('etape.libelleLong LIKE :search')
            ->setParameter('search', '%'.$term.'%')
            ->orderBy('etape.' . $champ, $ordre)
        ;

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /**
     * @param OffreDeFormationEtape $etape
     * @param OffreDeFormationDiplome $diplome
     * @return OffreDeFormationEtapeDiplome
     */
    public function getInfosByEtapeAndDiplome(OffreDeFormationEtape $etape, OffreDeFormationDiplome $diplome) {
        $qb = $this->getEntityManager()->getRepository(OffreDeFormationEtapeDiplome::class)->createQueryBuilder('ed')
            ->andWhere('ed.etape = :etape')
            ->andWhere('ed.diplome = :diplome')
            ->setParameter('etape', $etape)
            ->setParameter('diplome', $diplome)
        ;

        $result = $qb->getQuery()->getOneOrNullResult();
        return $result;
    }

}