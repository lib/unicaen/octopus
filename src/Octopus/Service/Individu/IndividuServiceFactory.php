<?php

namespace Octopus\Service\Individu;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;

class IndividuServiceFactory {

    public function __invoke(ContainerInterface $container)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get('doctrine.entitymanager.orm_octopus');

        /** @var IndividuService $service */
        $service = new IndividuService();
        $service->setEntityManager($entityManager);
        return $service;
    }
}