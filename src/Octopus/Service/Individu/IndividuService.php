<?php

namespace Octopus\Service\Individu;

use Doctrine\ORM\NonUniqueResultException;
use Octopus\Entity\Db\Individu;
use Octopus\Entity\Db\Source;
use UnicaenApp\Exception\RuntimeException;
use UnicaenApp\Service\EntityManagerAwareTrait;
use UnicaenUtilisateur\Entity\Db\User;

class IndividuService {
    use EntityManagerAwareTrait;
    
    /**
     * @param string $order
     * @return Individu[]
     */
    public function getIndividus($order = null)
    {
        $qb = $this->getEntityManager()->getRepository(Individu::class)->createQueryBuilder('individu');

        if($order)  $qb = $qb->orderBy('individu.' . $order);
        else        $qb = $qb->orderBy('individu.nomUsage, individu.prenom');

        $qb = $qb->setMaxResults(501);

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /**
     * @param int|null $id
     * @return Individu|null
     */
    public function getIndividu(?int $id) : ?Individu
    {
        $qb = $this->getEntityManager()->getRepository(Individu::class)->createQueryBuilder('individu')
            ->andWhere('individu.cIndividuChaine = :id')
            ->setParameter('id', (int) $id)
        ;

        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Individu partagent le même identifiant [".$id."].");
        }
        return $result;
    }

    /**
     * @param string $term
     * @param string $sourceCode
     * @return Individu[]
     */
    public function getIndividusByTerm($term, $sourceCode = null)
    {
        $term = strtolower($term);
        $qb = $this->getEntityManager()->getRepository(Individu::class)->createQueryBuilder('individu')
            ->addSelect('source')->join('individu.cSource', 'source')
            ->andWhere("   concat(concat(LOWER(individu.prenom),' '),LOWER(individu.nomUsage)) LIKE :search 
                        OR concat(concat(LOWER(individu.nomUsage),' '),LOWER(individu.prenom)) LIKE :search
                        OR concat(concat(LOWER(individu.prenom),' '),LOWER(individu.nomFamille)) LIKE :search
                        OR concat(concat(LOWER(individu.nomFamille),' '),LOWER(individu.prenom)) LIKE :search"
            )
            ->setParameter('search', '%'.$term.'%')
            ->orderBy('individu.nomUsage, individu.prenom')
        ;

        if ($sourceCode) {
            $qb = $qb->andWhere('source.cSource = :sourceCode')
                ->setParameter('sourceCode', $sourceCode);
        }

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /**
     * @param string $term
     * @return Individu[]
     */
    public function getEtudiantsByTerm($term)
    {
        $qb = $this->getEntityManager()->getRepository(Individu::class)->createQueryBuilder('individu')
            ->join('individu.affectations', 'affectation')
            ->andWhere('LOWER(CONCAT(individu.nomUsage, \' \' , individu.prenom, \' \', individu.nomUsage, \' \')) LIKE :search
                     OR LOWER(CONCAT(individu.nomFamille, \' \' , individu.prenom, \' \', individu.nomFamille, \' \')) LIKE :search
                     OR individu.cEtu LIKE :term
                    ')
            ->andWhere('affectation.type = :type')
            ->setParameter('search', '%'.strtolower($term).'%')
            ->setParameter('term', $term.'%')
            ->setParameter('type', 5)
            ->orderBy('individu.nomUsage, individu.prenom')
        ;

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /**
     * @param integer $numeroEtudiant
     * @return Individu
     */
    public function getEtudiantByNumero($numeroEtudiant)
    {
        $qb = $this->getEntityManager()->getRepository(Individu::class)->createQueryBuilder('individu')
            ->andWhere('individu.cEtu = :numero')
            ->setParameter('numero', $numeroEtudiant)
        ;

        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs étudiants partagent le même numéro étudiant [".$numeroEtudiant."].",$e);
        }
        return $result;
    }

}