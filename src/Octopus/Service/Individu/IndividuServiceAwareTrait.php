<?php

namespace Octopus\Service\Individu;

trait IndividuServiceAwareTrait {

    /** @var IndividuService */
    private $individuService;

    /**
     * @return IndividuService
     */
    public function getIndividuService()
    {
        return $this->individuService;
    }

    /**
     * @param IndividuService $individuService
     * @return IndividuService
     */
    public function setIndividuService($individuService)
    {
        $this->individuService = $individuService;
        return $this->individuService;
    }


}