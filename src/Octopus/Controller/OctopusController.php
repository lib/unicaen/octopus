<?php

namespace Octopus\Controller;

use Octopus\Service\Fonction\FonctionServiceAwareTrait;
use Octopus\Service\Geographie\GeographieServiceAwareTrait;
use Octopus\Service\Immobilier\ImmobilierServiceAwareTrait;
use Octopus\Service\Individu\IndividuServiceAwareTrait;
use Octopus\Service\OffreDeFormation\OffreDeFormationServiceAwareTrait;
use Octopus\Service\Structure\StructureServiceAwareTrait;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

class OctopusController extends AbstractActionController {
    use FonctionServiceAwareTrait;
    use GeographieServiceAwareTrait;
    use ImmobilierServiceAwareTrait;
    use IndividuServiceAwareTrait;
    use OffreDeFormationServiceAwareTrait;
    use StructureServiceAwareTrait;

    public function indexAction() {

        $structureTypes = $this->getStructureService()->getStructuresTypes('libelle');
        $structures     = $this->getStructureService()->getStructures('libelleCourt');

        $locals         = $this->getImmobiliserService()->getImmobilierLocals('libelle');
        $niveaux        = $this->getImmobiliserService()->getImmobilierNiveaux('libelle');
        $batiments      = $this->getImmobiliserService()->getImmobilierBatiments('libelle');
        $sites          = $this->getImmobiliserService()->getImmobilierSites('libelle');

        $individus      = $this->getIndividuService()->getIndividus();

        $fonctions              = $this->getFonctionService()->getFonctions();
        $fonctionsLibelles      = $this->getFonctionService()->getFonctionsLibelles();
        $fonctionsTypes         = $this->getFonctionService()->getFonctionsTypes();

        $pays           = $this->getGeographieService()->getPays('libelleCourt');

        $diplomes       = $this->getOffreDeFormationService()->getDiplomes();
        $etapes         = $this->getOffreDeFormationService()->getEtapes();
        
        return new ViewModel([
            'structureTypes' => $structureTypes,
            'structures' => $structures,

            'locals' => $locals,
            'niveaux' => $niveaux,
            'batiments' => $batiments,
            'sites' => $sites,

            'individus' => $individus,

            'fonctions' => $fonctions,
            'fonctionsLibelles' => $fonctionsLibelles,
            'fonctionsTypes' => $fonctionsTypes,

            'pays' => $pays,
            
            'diplomes' => $diplomes,
            'etapes' => $etapes,            
        ]);
    }
}