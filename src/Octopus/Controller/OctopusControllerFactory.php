<?php

namespace Octopus\Controller;

use Interop\Container\ContainerInterface;
use Octopus\Service\Fonction\FonctionService;
use Octopus\Service\Geographie\GeographieService;
use Octopus\Service\Immobilier\ImmobilierService;
use Octopus\Service\Individu\IndividuService;
use Octopus\Service\OffreDeFormation\OffreDeFormationService;
use Octopus\Service\Structure\StructureService;

class OctopusControllerFactory {

    public function __invoke(ContainerInterface $container)
    {
        /**
         * @var StructureService $structureService
         * @var ImmobilierService $immobilierService
         * @var IndividuService $individuService
         * @var GeographieService $geographieService
         * @var FonctionService $fonctionService
         * @var OffreDeFormationService $offreService
         */
        $structureService   = $container->get(StructureService::class);
        $immobilierService  = $container->get(ImmobilierService::class);
        $individuService    = $container->get(IndividuService::class);
        $geographieService  = $container->get(GeographieService::class);
        $fonctionService    = $container->get(FonctionService::class);
        $offreService       = $container->get(OffreDeFormationService::class);

        /** @var OctopusController $controller */
        $controller = new OctopusController();
        $controller->setStructureService($structureService);
        $controller->setImmobiliserService($immobilierService);
        $controller->setIndividuService($individuService);
        $controller->setGeographieService($geographieService);
        $controller->setFonctionService($fonctionService);
        $controller->setOffreDeFormationService($offreService);
        return $controller;
    }
}