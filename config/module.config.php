<?php

namespace Octopus;

use Doctrine\ORM\Mapping\Driver\XmlDriver;
use Doctrine\Persistence\Mapping\Driver\MappingDriverChain;
use Octopus\Controller\OctopusController;
use Octopus\Controller\OctopusControllerFactory;
use Octopus\Service\Fonction\FonctionService;
use Octopus\Service\Fonction\FonctionServiceFactory;
use Octopus\Service\Geographie\GeographieService;
use Octopus\Service\Geographie\GeographieServiceFactory;
use Octopus\Service\Immobilier\ImmobilierService;
use Octopus\Service\Immobilier\ImmobilierServiceFactory;
use Octopus\Service\Individu\IndividuService;
use Octopus\Service\Individu\IndividuServiceFactory;
use Octopus\Service\OffreDeFormation\OffreDeFormationService;
use Octopus\Service\OffreDeFormation\OffreDeFormationServiceFactory;
use Octopus\Service\Structure\StructureService;
use Octopus\Service\Structure\StructureServiceFactory;
//use UnicaenAuth\Guard\PrivilegeController;
use UnicaenPrivilege\Guard\PrivilegeController;
use Laminas\Router\Http\Literal;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => OctopusController::class,
                    'action'     => [
                        'index',
                    ],
                    'roles' => [],
                ]
            ],
        ],
    ],

    'doctrine' => [
        'driver' => [
            'orm_octopus' => [
                'class' => MappingDriverChain::class,
                'drivers' => [
                    'Octopus\Entity\Db' => 'orm_octopus_xml_driver',
                ],
            ],
            'orm_octopus_xml_driver' => [
                'class' => XmlDriver::class,
                'cache' => 'apc',
                'paths' => [
                    __DIR__ . '/../src/Octopus/Entity/Db/Mapping',
                ],
            ],
        ],
        'cache' => [
            'apc' => [
                'namespace' => 'PREECOG__' . __NAMESPACE__,
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'unicaen-octopus' => [
                'type' => Literal::class,
                'may_terminate' => true,
                'options' => [
                    'route'    => '/unicaen-octopus',
                    'defaults' => [
                        'controller' => OctopusController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            StructureService::class => StructureServiceFactory::class,
            ImmobilierService::class => ImmobilierServiceFactory::class,
            IndividuService::class => IndividuServiceFactory::class,
            GeographieService::class => GeographieServiceFactory::class,
            FonctionService::class => FonctionServiceFactory::class,
            OffreDeFormationService::class => OffreDeFormationServiceFactory::class,
        ],
    ],
    'controllers'     => [
        'factories' => [
            OctopusController::class => OctopusControllerFactory::class,
        ]
    ],

    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];
